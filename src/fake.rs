use std::ffi::OsStr;
use std::fmt::{Display, Formatter};
use std::io;
use std::io::{Error, ErrorKind};
use std::result::Result;

#[derive(Copy, Clone)]
pub struct FakeFile {}

impl FakeFile {
    pub fn write(&mut self, _buf: &[u8]) -> io::Result<usize> {
        unimplemented!()
    }

    pub fn read_to_string(&mut self, _buf: &mut String) -> io::Result<usize> {
        unimplemented!()
    }

    pub fn sync_all(&self) -> io::Result<()> {
        unimplemented!()
    }
}

pub struct FakeOpenOptions {}

impl FakeOpenOptions {
    pub fn new() -> FakeOpenOptions {
        FakeOpenOptions {}
    }

    pub fn read(&mut self, _read: bool) -> &mut FakeOpenOptions {
        self
    }

    pub fn write(&mut self, _read: bool) -> &mut FakeOpenOptions {
        self
    }

    pub fn create(&mut self, _read: bool) -> &mut FakeOpenOptions {
        self
    }

    pub fn open(&mut self, path: &FakePathBuf) -> io::Result<FakeFile> {
        match &path.file {
            Ok(file) => Ok(file.clone()),
            Err(error) => Err(Error::from(error.kind())),
        }
    }
}

pub struct FakePathBuf {
    pub entries: Result<FakeReadDir, Error>,
    pub file: Result<FakeFile, Error>,
    pub is_dir: bool,
}

impl Default for FakePathBuf {
    fn default() -> FakePathBuf {
        FakePathBuf {
            entries: Ok(FakeReadDir::from(&[])),
            file: Err(Error::from(ErrorKind::InvalidData)),
            is_dir: true,
        }
    }
}

impl Clone for FakePathBuf {
    fn clone(&self) -> Self {
        FakePathBuf {
            entries: match &self.entries {
                Ok(entries) => Ok(entries.clone()),
                Err(error) => Err(Error::from(error.kind())),
            },
            file: match &self.file {
                Ok(file) => Ok(file.clone()),
                Err(error) => Err(Error::from(error.kind())),
            },
            is_dir: self.is_dir,
        }
    }
}

impl FakePathBuf {
    pub fn entries(&self) -> Result<FakeReadDir, Error> {
        match &self.entries {
            Ok(read_dir) => Ok(read_dir.clone()),
            Err(error) => Err(Error::from(error.kind())),
        }
    }

    pub fn join(&self, _path: &String) -> FakePathBuf {
        unimplemented!()
    }

    pub fn display(&self) -> &str {
        unimplemented!()
    }

    pub fn file_name(&self) -> Option<&OsStr> {
        unimplemented!()
    }

    pub fn is_dir(&self) -> bool {
        self.is_dir
    }
}

pub struct FakeReadDir {
    dir_entries: Vec<FakeDirEntry>,
    index: usize,
}

impl FakeReadDir {
    pub fn from(dir_entries_array: &[FakeDirEntry]) -> Self {
        Self {
            dir_entries: dir_entries_array.to_vec(),
            index: 0,
        }
    }
}

impl Iterator for FakeReadDir {
    type Item = io::Result<FakeDirEntry>;

    fn next(&mut self) -> Option<io::Result<FakeDirEntry>> {
        let index = self.index;
        self.index = self.index + 1;
        match self.dir_entries.get(index) {
            None => None,
            Some(dir_entry) => Some(Ok(dir_entry.clone())),
        }
    }
}

impl Clone for FakeReadDir {
    fn clone(&self) -> Self {
        FakeReadDir {
            index: self.index,
            dir_entries: self.dir_entries.clone(),
        }
    }
}

pub struct FakeDirEntry {
    pub path: FakePathBuf,
}

impl FakeDirEntry {
    pub fn path(&self) -> FakePathBuf {
        self.path.clone()
    }
}

impl Display for FakeDirEntry {
    fn fmt(&self, _f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        unimplemented!()
    }
}

impl Clone for FakeDirEntry {
    fn clone(&self) -> Self {
        FakeDirEntry {
            path: self.path.clone(),
        }
    }
}

pub fn fake_read_dir(dir_path: &FakePathBuf) -> Result<FakeReadDir, Error> {
    match dir_path.entries() {
        Ok(entries) => Ok(entries),
        Err(e) => Err(e),
    }
}

pub fn fake_rename(_from: FakePathBuf, _to: &FakePathBuf) -> io::Result<()> {
    Ok(())
}
