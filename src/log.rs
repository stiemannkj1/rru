use std::env;
use std::env::VarError;
use std::sync::Once;

pub static VERBOSE_ARG: &str = "--verbose";

pub fn log_level() -> LogLevel {
    calculate_log_level_once(|| env::var("RUST_LOG"), env::args())
}

#[derive(PartialEq, Copy, Clone, Debug)]
pub enum LogLevel {
    DEBUG = 5,
    TRACE = 4,
    INFO = 3,
    WARN = 2,
    ERROR = 1,
    NONE = 0,
}

impl LogLevel {
    pub fn includes(self, expected: LogLevel) -> bool {
        self as u8 >= expected as u8
    }
}

/// The global log level. DO NOT ACCESS DIRECTLY, use log_level() instead.
static mut LOG_LEVEL: u8 = LogLevel::INFO as u8;
static INIT: Once = Once::new();

/// Testing Only
#[inline(always)]
fn calculate_log_level_once(
    log_level_getter: impl Fn() -> Result<String, VarError>,
    args: impl IntoIterator<Item = String>,
) -> LogLevel {
    unsafe {
        INIT.call_once(|| LOG_LEVEL = log_level_or_default(log_level_getter, args) as u8);
        LogLevel::from(LOG_LEVEL)
    }
}

/// Testing Only
#[inline(always)]
fn log_level_or_default(
    log_level_getter: impl Fn() -> Result<String, VarError>,
    args: impl IntoIterator<Item = String>,
) -> LogLevel {
    LogLevel::from(&log_level_getter().unwrap_or_else(|_| {
        let mut default_log_level = "INFO";
        for arg in args {
            if VERBOSE_ARG == arg {
                default_log_level = "DEBUG";
            }
        }
        default_log_level.to_string()
    }))
}

impl From<u8> for LogLevel {
    fn from(index: u8) -> Self {
        if index == LogLevel::DEBUG as u8 {
            LogLevel::DEBUG
        } else if index == LogLevel::TRACE as u8 {
            LogLevel::TRACE
        } else if index == LogLevel::INFO as u8 {
            LogLevel::INFO
        } else if index == LogLevel::WARN as u8 {
            LogLevel::WARN
        } else if index == LogLevel::ERROR as u8 {
            LogLevel::ERROR
        } else if index == LogLevel::NONE as u8 {
            LogLevel::NONE
        } else {
            panic!("Invalid index provided for LogLevel: {}", index)
        }
    }
}

impl From<&String> for LogLevel {
    fn from(log_level: &String) -> Self {
        match log_level.to_ascii_uppercase().as_str() {
            "DEBUG" => LogLevel::DEBUG,
            "TRACE" => LogLevel::TRACE,
            "INFO" => LogLevel::INFO,
            "WARN" => LogLevel::WARN,
            "ERROR" => LogLevel::ERROR,
            "NONE" => LogLevel::NONE,
            _ => panic!("Invalid string provided for LogLevel: {}", log_level),
        }
    }
}

#[macro_export]
macro_rules! debug {
    ($($arg:tt)*) => (if log::log_level().includes(log::LogLevel::DEBUG) { eprintln!($($arg)*); })
}

#[macro_export]
macro_rules! info {
    ($($arg:tt)*) => (if log::log_level().includes(log::LogLevel::INFO) { eprintln!($($arg)*); })
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::Cell;
    use std::iter::empty;

    const ALL_LOG_LEVELS: &[LogLevel] = &[
        LogLevel::DEBUG,
        LogLevel::TRACE,
        LogLevel::INFO,
        LogLevel::WARN,
        LogLevel::ERROR,
        LogLevel::NONE,
    ];
    const ALL_LOG_LEVELS_AS_STRINGS: &'static [&'static str] =
        &["DEBUG", "TRACE", "INFO", "WARN", "ERROR", "NONE"];

    #[test]
    fn it_calculates_log_level_once() {
        let called = Cell::new(0);
        let log_level_getter = || {
            called.set(called.get() + 1);
            Ok("NONE".to_string())
        };
        assert_eq!(
            LogLevel::NONE,
            calculate_log_level_once(log_level_getter, empty::<String>())
        );
        assert_eq!(
            LogLevel::NONE,
            calculate_log_level_once(log_level_getter, empty::<String>())
        );
        assert_eq!(
            LogLevel::NONE,
            calculate_log_level_once(log_level_getter, empty::<String>())
        );
        assert_eq!(
            LogLevel::NONE,
            calculate_log_level_once(log_level_getter, empty::<String>())
        );
        assert_eq!(
            LogLevel::NONE,
            calculate_log_level_once(log_level_getter, empty::<String>())
        );
        assert_eq!(1, called.get());
    }

    #[test]
    fn return_log_level() {
        assert_eq!(
            LogLevel::NONE,
            log_level_or_default(
                || Ok("NONE".to_string()),
                vec!["test".to_string(), "test".to_string(), "test".to_string()]
            )
        );
        assert_eq!(
            LogLevel::NONE,
            log_level_or_default(
                || Ok("NONE".to_string()),
                vec![
                    "test".to_string(),
                    "--verbose".to_string(),
                    "test".to_string()
                ]
            )
        );
        assert_eq!(
            LogLevel::NONE,
            log_level_or_default(|| Ok("NONE".to_string()), vec!["--verbose".to_string()])
        );
        assert_eq!(
            LogLevel::NONE,
            log_level_or_default(|| Ok("NONE".to_string()), empty::<String>())
        );
    }

    #[test]
    fn return_default_log_level_info_when_level_is_in_error() {
        assert_eq!(
            LogLevel::INFO,
            log_level_or_default(
                || Err(VarError::NotUnicode("".into())),
                vec!["test".to_string(), "test".to_string(), "test".to_string()]
            )
        );
        assert_eq!(
            LogLevel::INFO,
            log_level_or_default(|| Err(VarError::NotUnicode("".into())), empty::<String>())
        );
    }

    #[test]
    fn return_default_log_level_info_when_no_level_is_specified() {
        assert_eq!(
            LogLevel::INFO,
            log_level_or_default(
                || Err(VarError::NotPresent),
                vec!["test".to_string(), "test".to_string(), "test".to_string()]
            )
        );
        assert_eq!(
            LogLevel::INFO,
            log_level_or_default(|| Err(VarError::NotPresent), empty::<String>())
        );
    }

    #[test]
    fn return_default_log_level_debug_when_verbose_and_no_level_is_specified() {
        assert_eq!(
            LogLevel::DEBUG,
            log_level_or_default(
                || Err(VarError::NotPresent),
                vec![
                    "--verbose".to_string(),
                    "test".to_string(),
                    "test".to_string()
                ]
            )
        );
        assert_eq!(
            LogLevel::DEBUG,
            log_level_or_default(
                || Err(VarError::NotPresent),
                vec![
                    "test".to_string(),
                    "--verbose".to_string(),
                    "test".to_string()
                ]
            )
        );
        assert_eq!(
            LogLevel::DEBUG,
            log_level_or_default(
                || Err(VarError::NotPresent),
                vec![
                    "test".to_string(),
                    "test".to_string(),
                    "--verbose".to_string()
                ]
            )
        );
        assert_eq!(
            LogLevel::DEBUG,
            log_level_or_default(|| Err(VarError::NotPresent), vec!["--verbose".to_string()])
        );
    }

    #[test]
    fn log_levels_are_correctly_converted_from_u8() {
        for &log_level in ALL_LOG_LEVELS.iter() {
            assert_eq!(log_level, LogLevel::from(log_level as u8))
        }
    }

    #[test]
    #[should_panic]
    fn from_panics_when_invalid_u8_supplied() {
        LogLevel::from(ALL_LOG_LEVELS[0] as u8 + 1);
    }

    #[test]
    #[allow(non_snake_case)]
    fn log_levels_are_correctly_converted_from_String() {
        assert_eq!(ALL_LOG_LEVELS.len(), ALL_LOG_LEVELS_AS_STRINGS.len());
        for log_levels_and_strings in ALL_LOG_LEVELS.iter().zip(ALL_LOG_LEVELS_AS_STRINGS.iter()) {
            let (&log_level, &log_level_string) = log_levels_and_strings;
            assert_eq!(log_level, LogLevel::from(&log_level_string.to_string()));
            assert_eq!(
                log_level,
                LogLevel::from(&log_level_string.to_ascii_lowercase())
            );
            assert_eq!(
                log_level,
                LogLevel::from(&log_level_string.to_ascii_uppercase())
            );
        }
    }

    #[test]
    #[should_panic]
    fn from_panics_when_invalid_string_supplied() {
        LogLevel::from(&"test".to_string());
    }

    #[test]
    fn includes_correctly_detects_which_levels_are_currently_included() {
        for (i, &log_level) in ALL_LOG_LEVELS.iter().enumerate() {
            for (j, &expected_log_level) in ALL_LOG_LEVELS.iter().enumerate() {
                if i <= j {
                    assert!(log_level.includes(expected_log_level))
                } else {
                    assert!(!log_level.includes(expected_log_level))
                }
            }
        }
    }
}
