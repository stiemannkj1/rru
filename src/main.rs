#[macro_use]
mod log;
#[cfg(test)]
mod fake;

#[cfg(test)]
use crate::fake::fake_read_dir as read_dir;
#[cfg(test)]
use crate::fake::fake_rename as rename;
#[cfg(test)]
use crate::fake::FakeOpenOptions as OpenOptions;
#[cfg(test)]
use crate::fake::FakePathBuf as PathBuf;
#[cfg(not(test))]
use crate::log::log_level;
#[cfg(not(test))]
use std::env;
#[cfg(not(test))]
use std::fs::read_dir;
#[cfg(not(test))]
use std::fs::rename;
#[cfg(not(test))]
use std::fs::OpenOptions;
use std::io::Error;
#[cfg(not(test))]
use std::io::{Read, Write};
#[cfg(not(test))]
use std::path::PathBuf;

fn recursive_replace(dir_path: &PathBuf, search: &str, replace: &str) -> Result<(), Error> {
    let entries = match read_dir(dir_path) {
        Ok(entries) => entries,
        Err(error) => {
            debug!("Failed to read dir: {:?}", error);
            return Err(error);
        }
    };

    let mut replace_occurred = false;

    for entry in entries {
        let path = match entry {
            // TODO handle symlinks and named pipes.
            Ok(entry) => entry.path(),
            Err(error) => {
                debug!("Failed to access entry: {:?}", error);
                continue;
            }
        };

        if path.is_dir() {
            let _ = recursive_replace(&path, search, replace);
            continue;
        }

        let mut text = String::new();
        match OpenOptions::new()
            .read(true)
            .open(&path)
            .and_then(|mut file| file.read_to_string(&mut text))
        {
            Ok(_) => (),
            Err(error) => {
                debug!("Failed to read file: {:?}", error);
                continue;
            }
        }

        let replaced_text = text.replace(search, replace);

        if replaced_text == text {
            continue;
        }

        let mut i: u16 = 0;
        let file_name = match path.file_name().and_then(|file_name| file_name.to_str()) {
            Some(file_name) => file_name,
            None => {
                debug!("Failed to read file name: {}", path.display());
                continue;
            }
        };

        // Attempt to perform the replace atomically by:
        loop {
            // Creating a temp file on the same file system.
            let temp_file_path = dir_path.join(&[".", &i.to_string(), file_name].concat());
            match OpenOptions::new()
                .write(true)
                .create(true)
                .open(&temp_file_path)
                // Writing the replaced text to the temp file.
                .and_then(|mut temp_file| {
                    temp_file.write(replaced_text.as_bytes()).map(|_| temp_file)
                })
                .and_then(|temp_file| temp_file.sync_all().map(|_| temp_file))
                // Atomically moving the temp file to overwrite the original.
                .and_then(|_| rename(temp_file_path, &path))
            {
                Ok(_) => {
                    replace_occurred = true;
                    println!("{}", &path.display());
                    break;
                }
                // If the file already exists, try again with a different temp file name.
                Err(ref error) if error.kind() == std::io::ErrorKind::AlreadyExists => {
                    i = i + 1;
                    continue;
                }
                // TODO handle different failure types differently.
                Err(error) => {
                    debug!("Failed to replace text in file: {:?}", error);
                    break;
                }
            }
        }
    }

    if !replace_occurred {
        return Ok(());
    }

    match OpenOptions::new()
        .read(true)
        .open(dir_path)
        .and_then(|dir| dir.sync_all())
    {
        Ok(_) => Ok(()),
        Err(error) => {
            debug!("Failed to write replaced files to disk: {:?}", error);
            // Return Ok here because we might have been called recursively.
            // TODO rewrite this method to avoid recursion.
            Ok(())
        }
    }
}

#[cfg(not(test))]
fn main() -> Result<(), std::io::Error> {
    info!("Log level set to: {:?}", log_level());
    let mut search: Option<String> = Option::None;
    let mut replace: Option<String> = Option::None;
    let args: Vec<String> = env::args().collect();

    // Skip the first argument since it will always be the current utility.
    for arg in &args[1..] {
        debug!("Command line argument: \"{}\"", arg);
        if log::VERBOSE_ARG == arg.to_owned() {
            continue;
        }

        if search.is_none() {
            search = Option::Some(arg.to_owned());
        } else if replace.is_none() {
            replace = Option::Some(arg.to_owned());
        } else {
            panic!(
                "Unexpected argument found after search and replace arguments: {}",
                arg
            )
        }
    }

    recursive_replace(
        &env::current_dir()?,
        search.expect("Search string not provided.").as_str(),
        replace.expect("Replace string not provided.").as_str(),
    )
}

#[cfg(test)]
mod tests {
    use crate::fake::FakeReadDir;
    use crate::fake::{FakeDirEntry, FakePathBuf};
    use crate::recursive_replace;
    use std::io::{Error, ErrorKind};

    #[test]
    fn it_handles_errors_when_reading_dirs() {
        assert_eq!(
            ErrorKind::PermissionDenied,
            recursive_replace(
                &FakePathBuf {
                    entries: Err(Error::from(ErrorKind::PermissionDenied)),
                    is_dir: true,
                    ..FakePathBuf::default()
                },
                "test",
                "foo"
            )
            .unwrap_err()
            .kind()
        )
    }

    #[test]
    fn it_ignores_errors_in_subdirs() {
        let _ = match recursive_replace(
            &FakePathBuf {
                is_dir: true,
                entries: Ok(FakeReadDir::from(&[
                    FakeDirEntry {
                        path: FakePathBuf {
                            is_dir: true,
                            entries: Err(Error::from(ErrorKind::NotFound)),
                            ..FakePathBuf::default()
                        },
                    },
                    FakeDirEntry {
                        path: FakePathBuf {
                            is_dir: true,
                            entries: Err(Error::from(ErrorKind::PermissionDenied)),
                            ..FakePathBuf::default()
                        },
                    },
                ])),
                ..FakePathBuf::default()
            },
            "test",
            "foo",
        ) {
            Ok(_) => Ok(()),
            Err(_) => {
                assert!(false);
                Err(())
            }
        };
        ()
    }

    #[test]
    fn it_handles_errors_when_opening_files() {
        let _ = match recursive_replace(
            &FakePathBuf {
                is_dir: true,
                entries: Ok(FakeReadDir::from(&[FakeDirEntry {
                    path: FakePathBuf {
                        is_dir: false,
                        entries: Err(Error::from(ErrorKind::InvalidData)),
                        file: Err(Error::from(ErrorKind::InvalidData)),
                    },
                }])),
                ..FakePathBuf::default()
            },
            "test",
            "foo",
        ) {
            Ok(_) => Ok(()),
            Err(_) => {
                assert!(false);
                Err(())
            }
        };
        ()
    }

    #[test]
    fn no_errors_on_empty_dir() {
        let _ = match recursive_replace(
            &FakePathBuf {
                is_dir: true,
                ..FakePathBuf::default()
            },
            "test",
            "foo",
        ) {
            Ok(_) => Ok(()),
            Err(_) => {
                assert!(false);
                Err(())
            }
        };
        ()
    }
}
