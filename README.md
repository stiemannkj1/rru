# Recursive Replace Utility

Replaces strings in all files in a directory using regex. **Warning:** this is
alpha software. Use at your own risk.

## Building

[Install the `rust` toolchain.](https://www.rust-lang.org/tools/install)

Build:

```
cargo build
```
 
Test:

```
cargo test
```

Format:

```
cargo-fmt
```

## Usage

Replace all occurrences of `text` in a directory recursively:

```
rru text replacement
```

## License

Licensed under the MIT License.
